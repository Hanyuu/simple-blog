appdirs==1.4.0
Django==1.9.4
django-bootstrap3==8.1.0
django-ckeditor==5.2.1
gevent==1.2.1
greenlet==0.4.11
packaging==16.8
psycopg2==2.6.2
pyparsing==2.1.10
six==1.10.0
gunicorn==19.6.0

