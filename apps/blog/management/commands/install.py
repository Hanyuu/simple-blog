# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.core import management
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

class Command(BaseCommand):
    def handle(self,*args, **options):
        management.call_command('migrate', interactive=False)
        User.objects.create_superuser('test', 'test@test.com', 'test')


