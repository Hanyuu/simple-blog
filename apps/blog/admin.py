# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import BlogRecord,Comment
from django import forms
from ckeditor.widgets import CKEditorWidget
from ckeditor.fields import RichTextField

class BlogRecordAdminForm(forms.ModelForm):

    description = forms.CharField(required=False,widget=CKEditorWidget())

    content = forms.CharField(required=False,widget=CKEditorWidget())

    class Meta:
        fields = ('title','description','content',)
        model = BlogRecord

class BlogRecordAdmin(admin.ModelAdmin):
    form = BlogRecordAdminForm
admin.site.register(BlogRecord, BlogRecordAdmin)

class CommentRecordAdmin(admin.ModelAdmin):
    fields = ('user_name','message','blog_record',)
admin.site.register(Comment, CommentRecordAdmin)