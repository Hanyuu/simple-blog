$('document').ready(function() {
    var theme = $.cookie('theme');
    if(theme) {
        changeTheme('/static/css/' + theme + '.css')
    }

    $('#dark').bind( "click", function() {
        $.cookie('theme','dark');
        changeTheme('/st/css/dark.css')
    });

    $('#light').bind( "click", function() {
        $.cookie('theme','light');
        changeTheme('/st/css/light.css')
    });

    function changeTheme(theme_path) {
        var css_link=$('#theme');
        css_link.remove()
        css_link = $('<link id="theme" href="' + theme_path + '" id="dynamic-style-link" type="text/css" rel="stylesheet">');
        $('head').append(css_link);
        css_link.attr('href', theme_path);
    };


    $('img').bind('click',function(event){
        var modal_show_img=$('#Modal_show_img');
        modal_show_img.find('#img_show').attr('src',$(event.target).attr('src'));
        modal_show_img.modal('show');
    });
});s