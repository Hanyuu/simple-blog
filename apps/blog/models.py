# -*- coding: utf-8 -*-
from django.db import models


class BlogRecord(models.Model):

    title = models.CharField(u'Заголовок',null=False,blank=False,max_length=100)

    description = models.TextField(u'Описание',null=True,blank=True,max_length=500)

    content = models.TextField(u'Контент',max_length=1000,null=True,blank=True)

    date_publish = models.DateTimeField(u'Дата пудбликаци',auto_now=True)

    def __str__(self):
        return self.title


    def get_comments(self):
        return self.comments.all()


    def get_comments_count(self):
        return self.comments.all().count()

class Comment(models.Model):

    user_name = models.CharField(u'Имя',max_length=100,blank=False,null=False)

    message = models.CharField(u'Комментарий',max_length=100,blank=False,null=False)

    date_publish = models.DateTimeField(u'Дата пудбликаци', auto_now=True)

    blog_record = models.ForeignKey(BlogRecord,related_name='comments')

    def __str__(self):
        return '%s:%s'%(self.user_name,self.message,)
