# -*- coding: utf-8 -*-
from django.conf.urls import url,include
from apps.blog.views import IndexView,PostView
blog_urls = [

    url(r'^$', IndexView.as_view()),
    url(r'^(?P<page>\d+)/$',IndexView.as_view()),
    url(r'^post/(?P<post>\d+)/$',PostView.as_view()),
]