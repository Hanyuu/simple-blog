# -*- coding: utf-8 -*-
from django.views.generic import View
from django.shortcuts import render_to_response
from django.template import RequestContext
from django import http
from settings import settings
from apps.blog.models import BlogRecord,Comment
from django import forms
from ckeditor.widgets import CKEditorWidget
class AddCommentForm(forms.Form):

    user_name = forms.CharField(label=u'Имя', max_length=100)

    comment = forms.CharField(label=u'Комментарий',max_length=100,widget=CKEditorWidget())


class IndexView(View):

    def get(self, request, page=1):
        if page is None and not settings.LOCAL:
            return  http.HttpResponsePermanentRedirect('http://mpbat.xyz/1/')

        end = int(page) * 5

        start = end - 5

        records = BlogRecord.objects.all().order_by('-pk')[start:end]

        all_records = BlogRecord.objects.all().count()

        pages = self.generate_pages(all_records)

        return render_to_response('index.html',{'records':records,'pages':pages ,'current_page':int(page)}, RequestContext(request))

    def generate_pages(self,posts):
        pages = []
        i = 1
        pages.append(i)
        while (posts - (5*i)) > 0:
            i += 1
            pages.append(i)
        return pages

class PostView(View):

    form = AddCommentForm

    def get(self,request,post):

        record = BlogRecord.objects.get(pk=post)
        return render_to_response('post.html', {'record':record,'form':self.form(initial={'user_name':'Guest'})},RequestContext(request))

    def post(self,request,post):
        record = BlogRecord.objects.get(pk=post)
        form = self.form(request.POST)
        if form.is_valid():

            comment = Comment(blog_record=record,user_name=form.cleaned_data.get('user_name'), message=form.cleaned_data.get('comment'))

            comment.save()

            return render_to_response('post.html',{'record': record, 'form': self.form(initial={'user_name': 'Guest'})},RequestContext(request))
        else:
            return render_to_response('post.html',{'record': record, 'form': form},RequestContext(request))